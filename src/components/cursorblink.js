import React from "react";

class Cursorblink extends React.Component {
  state = {
    cursor: "|"
  };

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 500);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      cursor: this.state.cursor === "|" ? !this.state.cursor : "|"
    });
  }

  render() {
    return <>{this.state.cursor}</>;
  }
}

export default Cursorblink;
