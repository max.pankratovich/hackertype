import React from "react";
import mytext from "./text.js";
import "./style.css";
import Cursorblink from "./cursorblink";

class Hackertype extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: [],
      joinText: "",
      keyDownCount: 0,
      textDiv: null
    };
    this.setTextDivRef = element => {
      this.textDiv = element;
    };
  }

  onKeyPressed() {
    const joinText = [
      mytext[this.state.keyDownCount],
      mytext[this.state.keyDownCount + 1],
      mytext[this.state.keyDownCount + 2]
    ];

    this.setState(state => ({
      keyDownCount: state.keyDownCount + 3,
      text: [...state.text, joinText]
    }));
  }

  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  }

  componentDidMount() {
    this.textDiv.focus();
    
  }
  
  componentDidUpdate() {
    this.scrollToBottom();
  }

  render() {
    return (
      <div
        ref={this.setTextDivRef}
        className="hackerType"
        onKeyDown={() => this.onKeyPressed()}
        tabIndex="0"
      >
        {this.state.text}
        <Cursorblink />
        <div 
          classname="dummydiv"
          ref={(el) => { this.messagesEnd = el; }}
        >

        </div>
      </div>
    );
  }
}

export default Hackertype;
