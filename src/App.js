import React from "react";

import Hackertype from "./components/hackertype";

function App() {
  return (
    <div>
      <Hackertype />
    </div>
  );
}

export default App;
